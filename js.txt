$(document).ready(function () {

	// Collapsible menu tree
    $('a.tree-toggler').click(function () {
        $(this).parent().children('ul.tree').toggle(300);
    });



    //===================================================================================//
    							//	Clients  // 
    //===================================================================================//


    // add client form validation
    $("#addClientForm").unbind('submit').bind('submit', function() {
		$(".text-danger").remove();
		$('.form-group').removeClass('has-error').removeClass('has-success');
		
		var clientName   = $("#clientName").val();
		var clientCode   = $("#clientCode").val();
		var statusCode   = $("#statusCode").val();

		if(clientName == "") {
			$("#clientName").after('<p class="text-danger">Please Provide Client Name.</p>');
			$("#clientName").closest('.form-group').addClass('has-error');
		} else {
			$("#clientName").find('.text-danger').remove();
			$("#clientName").closest('.form-group').addClass('has-success');
		}
		

		if(clientCode == "") {
			$("#clientCode").after('<p class="text-danger">Please Provide Client Code.</p>');
			$("#clientCode").closest('.form-group').addClass('has-error');
		} else {
			$("#clientCode").find('.text-danger').remove();
			$("#clientCode").closest('.form-group').addClass('has-success');
		}

		if(statusCode == "") {
			$("#statusCode").after('<p class="text-danger">Please Choose Client Status.</p>');
			$("#statusCode").closest('.form-group').addClass('has-error');
		} else {
			$("#statusCode").find('.text-danger').remove();
			$("#statusCode").closest('.form-group').addClass('has-success');
		}

		if(clientName && clientCode && statusCode) {
			return true;
		} else {
			return false;
		}
	});// end of submit add client form


    // onclick edit client button
	$('.edit-client').click(function(){
        
        // get params on btn edit client click and populate fields on modal
        var clientID = $(this).closest('tr').children('td.clientID').text();
        $('#eclientID').val(clientID);
        
        var clientName = $(this).closest('tr').children('td.clientName').text();
        $('#eclientName').val(clientName);
        
        var status = $(this).closest('tr').children('input.status').val();
        $('#eclientStatus').val(status);
        
        var clientCode = $(this).closest('tr').children('td.clientCode').text();
        $('#eclientCode').val(clientCode);
        
        var date_created = $(this).closest('tr').children('td.date_created').text();
        $('#edate_created').val(date_created);

        var date_modified = $(this).closest('tr').children('td.date_modified').text();
        $('#edate_modified').val(date_modified);

        $("#editClientModal").find("form").attr("action",'/clients/' + clientID);

    });

	// onclick deactivate client button
	$('.deactivate-client').click(function(){
        
        // get params on btn deactivate client click and populate fields on modal
        var clientID = $(this).closest('tr').children('td.clientID').text();
        $('#rclientID').val(clientID);

        $("#deactivateClientModal").find("form").attr("action",'/clients/' + clientID);

    });


     // add client form validation
    $("#editClientForm").unbind('submit').bind('submit', function() {
		$(".text-danger").remove();
		$('.form-group').removeClass('has-error').removeClass('has-success');
		
		var eclientName   = $("#eclientName").val();
		var eclientCode   = $("#eclientCode").val();
		var eclientStatus   = $("#eclientStatus").val();

		if(eclientName == "") {
			$("#eclientName").after('<p class="text-danger">Please Provide Client Name.</p>');
			$("#eclientName").closest('.form-group').addClass('has-error');
		} else {
			$("#eclientName").find('.text-danger').remove();
			$("#eclientName").closest('.form-group').addClass('has-success');
		}
		

		if(eclientCode == "") {
			$("#eclientCode").after('<p class="text-danger">Please Provide Client Code.</p>');
			$("#eclientCode").closest('.form-group').addClass('has-error');
		} else {
			$("#eclientCode").find('.text-danger').remove();
			$("#eclientCode").closest('.form-group').addClass('has-success');
		}

		if(eclientStatus == "") {
			$("#eclientStatus").after('<p class="text-danger">Please Choose Client Status.</p>');
			$("#eclientStatus").closest('.form-group').addClass('has-error');
		} else {
			$("#eclientStatus").find('.text-danger').remove();
			$("#eclientStatus").closest('.form-group').addClass('has-success');
		}

		if(eclientName && eclientCode && eclientStatus) {
			return true;
		} else {
			return false;
		}
	});// end of submit edit client form







    //===================================================================================//
    							//	Client Channel  // 
    //===================================================================================//

	 // add client channel form validation
    $("#addClientChannelForm").unbind('submit').bind('submit', function() {
		$(".text-danger").remove();
		$('.form-group').removeClass('has-error').removeClass('has-success');
		
		var client          = $("#client").val();
		var clientChannel   = $("#clientChannel").val();
		var clientChannelStatusCode = $("#clientChannelStatusCode").val();
		var clientChannelName = $("#clientChannelName").val();

		if(client == "") {
			$("#client").after('<p class="text-danger">Please Choose Client Name.</p>');
			$("#client").closest('.form-group').addClass('has-error');
		} else {
			$("#client").find('.text-danger').remove();
			$("#client").closest('.form-group').addClass('has-success');
		}
		

		if(clientChannel == "") {
			$("#clientChannel").after('<p class="text-danger">Please Choose Channel.</p>');
			$("#clientChannel").closest('.form-group').addClass('has-error');
		} else {
			$("#clientChannel").find('.text-danger').remove();
			$("#clientChannel").closest('.form-group').addClass('has-success');
		}

		if(clientChannelStatusCode == "") {
			$("#clientChannelStatusCode").after('<p class="text-danger">Please Choose Client Channel Status.</p>');
			$("#clientChannelStatusCode").closest('.form-group').addClass('has-error');
		} else {
			$("#clientChannelStatusCode").find('.text-danger').remove();
			$("#clientChannelStatusCode").closest('.form-group').addClass('has-success');
		}

		if(clientChannelName == "") {
			$("#clientChannelName").after('<p class="text-danger">Please Provide Client Channel Name.</p>');
			$("#clientChannelName").closest('.form-group').addClass('has-error');
		} else {
			$("#clientChannelName").find('.text-danger').remove();
			$("#clientChannelName").closest('.form-group').addClass('has-success');
		}

		if(client && clientChannel && clientChannelStatusCode && clientChannelName) {
			return true;
		} else {
			return false;
		}
	});// end of submit add client channel form


    // onclick edit client channel button
	$('.edit-client-channel').click(function(){
        
        // get params on btn edit client click and populate fields on modal
        var clientChannelID = $(this).closest('tr').children('td.eclient_channelID').text();
        $('#eclient_channelID').val(clientChannelID);
        
        var client = $(this).closest('tr').children('input.clientID').val();
        $('#eclient').val(client);

        var cchannelID = $(this).closest('tr').children('input.clientChannelChannelID').val();
        $('#cechannelID').val(cchannelID);

        var channelName = $(this).closest('tr').children('td.channelName').text();
        $('#eclientChannelName').val(channelName);
        
        var clientChannel = $(this).closest('tr').children('td.clientChannel').text();
        $('#eclientChannel').val(clientChannel);
        
        var clientChannelStatusCode = $(this).closest('tr').children('input.clientChannelStatusCode').val();
        $('#eclientChannelStatusCode').val(clientChannelStatusCode);
        
        var clientChannelName = $(this).closest('tr').children('td.clientChannelName').text();
        $('#eclientChannelName').val(clientChannelName);
        

        var clientChanneldate_created = $(this).closest('tr').children('td.clientChanneldate_created').text();
        $('#eclientChanneldate_created').val(clientChanneldate_created);

        var clientChanneldate_modified = $(this).closest('tr').children('td.clientChanneldate_modified').text();
        $('#eclientChanneldate_modified').val(clientChanneldate_modified);

        $("#editClientChannelModal").find("form").attr("action",'/client_channel/' + cchannelID);

    });

	// onclick deactivate client channel button
	$('.deactivate-client-channel').click(function(){
        
        // get params on btn deactivate client channel click and populate fields on modal
        var clientChannelID = $(this).closest('tr').children('td.eclient_channelID').text();
        $('#rclientChannelID').val(clientChannelID);

        $("#deactivateClientChannelModal").find("form").attr("action",'/client_channel/' + clientChannelID);

    });


     // edit client channel form validation
    $("#editClientChannelForm").unbind('submit').bind('submit', function() {
		$(".text-danger").remove();
		$('.form-group').removeClass('has-error').removeClass('has-success');
		
		var eclient                  = $("#eclient").val();
		var cechannelID              = $("#cechannelID").val();
		var eclientChannelStatusCode = $("#eclientChannelStatusCode").val();
		var eclientChannelName       = $("#eclientChannelName").val();
		var eclientStatus            = $("#eclientStatus").val();

		if(eclient == "") {
			$("#eclient").after('<p class="text-danger">Please Choose Client Name.</p>');
			$("#eclient").closest('.form-group').addClass('has-error');
		} else {
			$("#eclient").find('.text-danger').remove();
			$("#eclient").closest('.form-group').addClass('has-success');
		}
		

		if(cechannelID == "") {
			$("#cechannelID").after('<p class="text-danger">Please Choose Channel.</p>');
			$("#cechannelID").closest('.form-group').addClass('has-error');
		} else {
			$("#cechannelID").find('.text-danger').remove();
			$("#cechannelID").closest('.form-group').addClass('has-success');
		}

		if(eclientChannelStatusCode == "") {
			$("#eclientChannelStatusCode").after('<p class="text-danger">Please Choose Client Channel Status Code.</p>');
			$("#eclientChannelStatusCode").closest('.form-group').addClass('has-error');
		} else {
			$("#eclientChannelStatusCode").find('.text-danger').remove();
			$("#eclientChannelStatusCode").closest('.form-group').addClass('has-success');
		}

		if(eclientChannelName == "") {
			$("#eclientChannelName").after('<p class="text-danger">Please Provide Client Channel Name.</p>');
			$("#eclientChannelName").closest('.form-group').addClass('has-error');
		} else {
			$("#eclientChannelName").find('.text-danger').remove();
			$("#eclientChannelName").closest('.form-group').addClass('has-success');
		}


		if(eclient && echannelID && eclientChannelStatusCode && eclientChannelName) {
			return true;
		} else {
			return false;
		}
	});// end of submit edit client channel form


    //===================================================================================//
    							//	Status Codes  // 
    //===================================================================================//

	 // add status code form validation
    $("#addStatusCodeForm").unbind('submit').bind('submit', function() {
		$(".text-danger").remove();
		$('.form-group').removeClass('has-error').removeClass('has-success');
		
		var code          = $("#code").val();
		var description   = $("#description").val();
	
		if(code == "") {
			$("#code").after('<p class="text-danger">Please Provide Status Code.</p>');
			$("#code").closest('.form-group').addClass('has-error');
		} else {
			$("#code").find('.text-danger').remove();
			$("#code").closest('.form-group').addClass('has-success');
		}
		

		if(description == "") {
			$("#description").after('<p class="text-danger">Please Provide Description.</p>');
			$("#description").closest('.form-group').addClass('has-error');
		} else {
			$("#description").find('.text-danger').remove();
			$("#description").closest('.form-group').addClass('has-success');
		}

	
		if(code && description) {
			return true;
		} else {
			return false;
		}
	});// end of submit add status code form


    // onclick edit status code button
	$('.edit-status-code').click(function(){
        
        // get params on btn edit status code click and populate fields on modal
        var statusCodeID = $(this).closest('tr').children('td.statusCodeID').text();
        $('#estatusCodeID').val(statusCodeID);
        
        var code = $(this).closest('tr').children('td.code').text();
        $('#ecode').val(code);

        var description = $(this).closest('tr').children('td.description').text();
        $('#edescription').val(description);

       

        $("#editStatusCodeModal").find("form").attr("action",'/status_codes/' + statusCodeID);

    });

	// onclick deactivate status code button
	$('.deactivate-status-code').click(function(){
        
        // get params on btn deactivate client channel click and populate fields on modal
        var statusCodeID = $(this).closest('tr').children('td.statusCodeID').text();
        $('#rstatusCodeID').val(statusCodeID);

        $("#deactivateStatusCodeModal").find("form").attr("action",'/status_codes/' + statusCodeID);

    });


     // edit status code form validation
    $("#editStatusCodeForm").unbind('submit').bind('submit', function() {
		$(".text-danger").remove();
		$('.form-group').removeClass('has-error').removeClass('has-success');
		
		var code          = $("#ecode").val();
		var description   = $("#edescription").val();
	
		if(code == "") {
			$("#ecode").after('<p class="text-danger">Please Provide Status Code.</p>');
			$("#ecode").closest('.form-group').addClass('has-error');
		} else {
			$("#ecode").find('.text-danger').remove();
			$("#ecode").closest('.form-group').addClass('has-success');
		}
		

		if(description == "") {
			$("#edescription").after('<p class="text-danger">Please Provide Description.</p>');
			$("#edescription").closest('.form-group').addClass('has-error');
		} else {
			$("#edescription").find('.text-danger').remove();
			$("#edescription").closest('.form-group').addClass('has-success');
		}

	
		if(code && description) {
			return true;
		} else {
			return false;
		}
	});// end of submit edit status code form


    //===================================================================================//
    							//	Channels  // 
    //===================================================================================//

	 // add channel form validation
    $("#addChannelForm").unbind('submit').bind('submit', function() {
		$(".text-danger").remove();
		$('.form-group').removeClass('has-error').removeClass('has-success');
		
		var channelName       = $("#channelName").val();
		var channelStatusCode = $("#channelStatusCode").val();
		var channelCode = $("#channelCode").val();
	
		if(channelName == "") {
			$("#channelName").after('<p class="text-danger">Please Provide Channel Name.</p>');
			$("#channelName").closest('.form-group').addClass('has-error');
		} else {
			$("#channelName").find('.text-danger').remove();
			$("#channelName").closest('.form-group').addClass('has-success');
		}
		

		if(channelStatusCode == "") {
			$("#channelStatusCode").after('<p class="text-danger">Please Choose Status Code.</p>');
			$("#channelStatusCode").closest('.form-group').addClass('has-error');
		} else {
			$("#channelStatusCode").find('.text-danger').remove();
			$("#channelStatusCode").closest('.form-group').addClass('has-success');
		}

		if(channelCode == "") {
			$("#channelCode").after('<p class="text-danger">Please Provide Channel Code.</p>');
			$("#channelCode").closest('.form-group').addClass('has-error');
		} else {
			$("#channelCode").find('.text-danger').remove();
			$("#channelCode").closest('.form-group').addClass('has-success');
		}

	
		if(channelName && channelStatusCode && channelCode) {
			return true;
		} else {
			return false;
		}
	});// end of submit add channel form


    // onclick edit channel button
	$('.edit-channel').click(function(){
        
        // get params on btn edit channel click and populate fields on modal
        var channelID = $(this).closest('tr').children('td.channelID').text();
        $('#echannelID').val(channelID);
        
        var channelName = $(this).closest('tr').children('td.channelName').text();
        $('#echannelName').val(channelName);

        var channelStatusCode = $(this).closest('tr').children('input.channelStatusCode').val();
        $('#echannelStatusCode').val(channelStatusCode);

        var channelCode = $(this).closest('tr').children('td.channelCode').text();
        $('#echannelCode').val(channelCode);

       
        $("#editChannelModal").find("form").attr("action",'/channels/' + channelID);

    });

	// onclick deactivate channel button
	$('.deactivate-channel').click(function(){
        
        // get params on btn deactivate channel click and populate fields on modal
        var rchannelID = $(this).closest('tr').children('td.rchannelID').text();
        $('#rchannelID').val(rchannelID);

        $("#deactivateChannelModal").find("form").attr("action",'/channels/' + rchannelID);

    });


     // edit channel form validation
    $("#editChannelModal").unbind('submit').bind('submit', function() {
		$(".text-danger").remove();
		$('.form-group').removeClass('has-error').removeClass('has-success');
		
		var channelName       = $("#echannelName").val();
		var channelStatusCode = $("#echannelStatusCode").val();
		var channelCode = $("#echannelCode").val();
	
		if(channelName == "") {
			$("#echannelName").after('<p class="text-danger">Please Provide Channel Name.</p>');
			$("#echannelName").closest('.form-group').addClass('has-error');
		} else {
			$("#echannelName").find('.text-danger').remove();
			$("#echannelName").closest('.form-group').addClass('has-success');
		}
		

		if(channelStatusCode == "") {
			$("#echannelStatusCode").after('<p class="text-danger">Please Choose Status Code.</p>');
			$("#echannelStatusCode").closest('.form-group').addClass('has-error');
		} else {
			$("#echannelStatusCode").find('.text-danger').remove();
			$("#echannelStatusCode").closest('.form-group').addClass('has-success');
		}

		if(channelCode == "") {
			$("#echannelCode").after('<p class="text-danger">Please Provide Channel Code.</p>');
			$("#echannelCode").closest('.form-group').addClass('has-error');
		} else {
			$("#echannelCode").find('.text-danger').remove();
			$("#echannelCode").closest('.form-group').addClass('has-success');
		}

	
		if(channelName && channelStatusCode && channelCode) {
			return true;
		} else {
			return false;
		}
	});// end of submit edit channel form

		
});

